<?php
include_once '../../view/font/include/header.php';
?>
<!-- Top isues start here-->
<div class="container">

    <h1>Top Stories</h1><br>
    <div class="container ">
        <div class="row">


            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Save Rohingyas</h4>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" class="btn btn-danger">Donate Now!</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Help Flood-affected People</h4>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" class="btn btn-danger">Donate Now!</a>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <hr>


            <br>

            <br>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Patronize a Genius of Bagerhat</h4>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" class="btn btn-danger">Donate Now!</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Save Ayesha!</h4>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" class="btn btn-danger">Donate Now!</a>
                        </div>
                    </div>
                </div>
            </div>




        </div>
    </div> <br>
    <hr>
    <hr>
    <nav aria-label="Page navigation example" >
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
        </ul>
    </nav>
</div>

<!------------------------FOOTER-AREA START--------------------->
<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                <div class="single-footer-widget">
                    <div class="footer-logo">
                        <h3>Fund Management</h3>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, suscipit iste minus itaque voluptate quod, quo ex, a delectus distinctio sapiente officia! Ex distinctio, ratione odit doloremque dolores quidem ipsum!</p>
                    <div class="footer-social-icon">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </div>
                </div>
                <!-- .single-footer-widget -->
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-footer-widget">
                    <h4 class="footer-widget-title">Importaint link</h4>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">about</a></li>
                        <li><a href="#">service</a></li>
                        <li><a href="#">course</a></li>
                        <li><a href="#">testimonial</a></li>
                        <li><a href="#">contact</a></li>
                    </ul>
                </div>
                <!-- .single-footer-widget -->
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-footer-widget">
                    <h4 class="footer-widget-title">contact us</h4>
                    <p><strong>Location</strong> Dhanmondi - 32, Dhaka <br> Dhaka - 1215</p>
                    <p><strong>Email</strong> education@gmail.com
                    </p>
                    <p><strong>Phone</strong> +8801723333608
                    </p>
                </div>
                <!-- .single-footer-widget -->
            </div>
        </div>
    </div>
</div>
<div class="footer-copyright-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="copyright">
                    <p>Copyright <span>&copy;</span> 2017, All Right Reserved</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="design-by">
                    <p>Designed by <span>Imran Hoshain</span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!------------------------FOOTER-AREA END--------------------->
