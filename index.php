<?php
include_once 'view/font/include/header.php';
include_once 'vendor/autoload.php';
/*$bazar = new App\admin\bazar\Bazar;
$data = $bazar->index();*/
$admin = new App\Admin\Admin();
$admins = $admin->index();
?>

    <!------------------------SLIDER-AREA START--------------------->
    <div class="slider-area">
        <div class="slider">
            <div class="single-slider" style="background:url(img/slide3.jpg) no-repeat scroll center top / cover;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8">
                            <div class="slider-content">
                                <h3>Best for you</h3>
                                <h2>Web design course</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos iusto aspernatur sunt corrupti voluptates qui nobis voluptatem ab necessitatibus aliquam!</p>
                                <a href="#" class="slide-btn">Learn more</a>
                                <a href="#" class="slide-btn">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .single-slider -->
            <div class="single-slider" style="background:url(img/slide2.jpg) no-repeat scroll center top / cover;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8">
                            <div class="slider-content">
                                <h3>Best for you</h3>
                                <h2>graphice design course</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos iusto aspernatur sunt corrupti voluptates qui nobis voluptatem ab necessitatibus aliquam!</p>
                                <a href="#" class="slide-btn">Learn more</a>
                                <a href="#" class="slide-btn">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .single-slider -->
            <div class="single-slider" style="background:url(img/slide1.jpg) no-repeat scroll center top / cover;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8">
                            <div class="slider-content">
                                <h3>Best for you</h3>
                                <h2>wordpress course</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos iusto aspernatur sunt corrupti voluptates qui nobis voluptatem ab necessitatibus aliquam!</p>
                                <a href="#" class="slide-btn">Learn more</a>
                                <a href="#" class="slide-btn">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .single-slider -->
        </div>
    </div>
    <!------------------------SLIDER-AREA END--------------------->

    <!------------------------COURSE-AREA START--------------------->
    <div class="course-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Event Detail</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa <br> tempora, architecto, expedita beatae quae cumque!</p>
                    </div>
                </div>
            </div>
            <box>
                <?php
                $sl = 1;
                foreach ($admins as $admin){
                    ?>

                    <div class="col-md-4">
                        <div class="layer">
                            <img height="200" src="assets/uploads/<?= $admin['image']?>" alt="image loading...">
                            <h4>Event Name: <?= $admin['name']?></h4>

                            Details:
                            <p>
                                Starting Date: <?= $admin['start_date']?><br>
                                Ending Date: <?= $admin['end_date']?><br>
                                Area: <?= $admin['location']?><br>
                                Paying Methods: <?= $admin['pay_method']?>
                            </p>
                            <hr class="line">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="price">Target: ৳ <?= $admin['target']?> </p>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                <?php }?>

            </box>
        </div>
    </div>
    <!------------------------COURSE-AREA END--------------------->

    <!------------------------COUNTER-AREA START--------------------->
    <div class="counter-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="single-counter">
                        <div class="counter-img">
                            <img src="img/counter1.png" alt="">
                        </div>
                        <div class="counter-detail">
                            <h4 class="counter">500</h4>
                            <p>Help People</p>
                        </div>
                    </div>
                    <!-- .single-counter -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="single-counter">
                        <div class="counter-img">
                            <img src="img/counter2.png" alt="">
                        </div>
                        <div class="counter-detail">
                            <h4 class="counter">140000</h4>
                            <p>Tergate Money</p>
                        </div>
                    </div>
                    <!-- .single-counter -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="single-counter">
                        <div class="counter-img">
                            <img src="img/counter3.png" alt="">
                        </div>
                        <div class="counter-detail">
                            <h4 class="counter">20000</h4>
                            <p>Neded Money</p>
                        </div>
                    </div>
                    <!-- .single-counter -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="single-counter">
                        <div class="counter-img">
                            <img src="img/counter4.png" alt="">
                        </div>
                        <div class="counter-detail">
                            <h4 class="counter">4 Days</h4>
                            <p>Day Remaining</p>
                        </div>
                    </div>
                    <!-- .single-counter -->
                </div>
            </div>
        </div>
    </div>
    <!------------------------COUNTER-AREA END--------------------->

    <!------------------------TRAINER-AREA START--------------------->
    <div class="trainer-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Our Team</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa <br> tempora, architecto, expedita beatae quae cumque!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-trainer">
                        <img src="img/trainer1.jpg" alt="">
                        <div class="trainer-detail">
                            <h4>Imran Hossain <span>Not Eligiable</span></h4>
                            <div class="trainer-social-icon">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- .single-trainer -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-trainer">
                        <img src="img/trainer1.jpg" alt="">
                        <div class="trainer-detail">
                            <h4>Tajim <span>Not Eligiable</span></h4>
                            <div class="trainer-social-icon">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- .single-trainer -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-trainer">
                        <img src="img/trainer1.jpg" alt="">
                        <div class="trainer-detail">
                            <h4>Shovik <span>Not Eligiable</span></h4>
                            <div class="trainer-social-icon">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- .single-trainer -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-trainer">
                        <img src="img/trainer1.jpg" alt="">
                        <div class="trainer-detail">
                            <h4>Momin <span>Not Eligiable</span></h4>
                            <div class="trainer-social-icon">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- .single-trainer -->
                </div>     
            </div>
        </div>
    </div>
    <!------------------------TRAINER-AREA END--------------------->

   

    <!------------------------QUOTE-AREA START--------------------->
    <div class="quote-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h2>You start professional course with us!</h2>
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="contact.html" class="quote-btn">Contact us</a>
                </div>
            </div>
        </div>
    </div>
    <!------------------------QUOTE-AREA END--------------------->

    <!------------------------NEWS-AREA START--------------------->
    <div class="news-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>News Of Help People</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa <br> tempora, architecto, expedita beatae quae cumque!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="single-news">
                        <div class="news-img">
                            <img src="img/news1.jpg" alt="">
                        </div>
                        <div class="news-detail">
                            <h4>Help Title</h4>
                            <div class="news-meta">
                                <span><i class="fa fa-user"></i> News Paper</span> <span><i class="fa fa-calendar"></i> 5 december 2017</span> <span><i class="fa fa-heart"></i> 20</span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi omnis iste perferendis ut numquam debitis nostrum est alias sit, sed quisquam, eaque, quam nam eveniet laboriosam mollitia quas vel inventore.</p>
                            <a href="#" class="edu-btn">Read more <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <!-- .single-news -->
                </div>   		
            </div>
        </div>
    </div>
    <!------------------------NEWS-AREA END--------------------->


<?php
include_once 'view/font/include/footer.php';
?>